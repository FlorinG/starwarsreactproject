The goal of this test project  is to illustrate the knowledge in using React.
Resource:
1. Use SWAPI (https://swapi.py4e.com/) as a sample API – just for fetching the results/information,
not to use built-in search or pagination provided by SWAPI, use client side custom solution
The app should contain:
2. A home page showing two categories of items
3. A category page (with pagination) to show all of its items
4. A details page of one item showing more detailed information

5. Search (for one category)
6. One or two filtering options (for one category)

7. A minimum of clean custom CSS

Choose one of two cathegory for browsing the data.
In People page, you will be able to filter data on genre.
In Planet page, if select a planet name, then more informations will be displayed. 