import React, { Component } from 'react'
import PlanetTable from "./PlanetTable.js";
import ReactDOM from 'react-dom';

class PlanetDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            planetSelect: "",
        };
    }

    getAllPlanets() {
        ReactDOM.render(<PlanetTable />, document.getElementById("root"));
    }



    render() {
        console.log(this.props.planetSelect)
        let neObj = Object.keys(this.props.planetSelect).map(item => `${item}: ${this.props.planetSelect[item]}`)
        console.log(neObj)

        return (
            <div id="detailsTable">
                <center>
                    <h2>Planet details</h2>
                    <table>
                        <tbody >
                            {neObj.map(item =>
                                <tr>
                                    <td>{item}</td>
                                </tr>)}
                        </tbody>
                    </table>
                    <button onClick={this.getAllPlanets}>Back to planet list</button>
                </center>
            </div>
        )
    }
}

export default PlanetDetails