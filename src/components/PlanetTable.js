import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import PlanetDetails from "./PlanetDetails.js";
import Resources from "../index.js";

var allData = ["dummy"];
var itemsPerPage = 15;
var nrPages = 1;
var firstInPage = 0;
var lastInPage = 0;
var pageCrt = 1;

const BASE_URL = "https://swapi.py4e.com/api/planets/";

async function fetchData(apiUrl) {
    allData.length = 0;
    while (apiUrl !== '' && apiUrl != null) {
        await fetch(apiUrl)
            .then(res => res.json())
            .then(data => {
                allData.push(...data.results);
                apiUrl = data['next'];
                console.log(apiUrl);
            })
    }
}

class PlanetTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            planete: [],
            inputName: "",
            planetSelect: {},
            currentPage: 1,
        };
    }

    nextPage = () => {

        console.log(this.state.currentPage)
        pageCrt = this.state.currentPage;
        if (this.state.currentPage < nrPages) {
            this.setState({ currentPage: pageCrt + 1 })
        }
    }

    backPage = () => {
        console.log(this.state.currentPage)
        pageCrt = this.state.currentPage;
        if (this.state.currentPage > 1) {
            this.setState({ currentPage: pageCrt - 1 })
        }
    }

    hasName = (myObj) => this.state.inputName === myObj.name;

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({ inputName: event.target.name.value });
    }

    backToMenu() {
        ReactDOM.render(<Resources />, document.getElementById("root"));
    }

    render() {
        const { planete } = this.state;
        const lastItem = this.state.planete.length - 1;
        nrPages = Math.ceil(planete.length / itemsPerPage);
        console.log(nrPages);
        firstInPage = (this.state.currentPage - 1) * itemsPerPage;
        lastInPage = Math.min(firstInPage + itemsPerPage, lastItem);
        let button = "";
        if (this.state.planete.find(this.hasName) !== undefined) {
            console.log(this.state.planetSelect)
            button = <button onClick=
                {ReactDOM.render(<PlanetDetails planetSelect={this.state.planete.find(this.hasName)} />
                    , document.getElementById("root"))}>View details</button>
        }
        return (
            <div id="listingTable">
                <center>
                    <h2>Planets</h2>
                    <h3>For detailed informations, please submit a planet name:</h3>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            Planet name:
                        <input
                                type="text"
                                name="name"
                            />
                        </label>
                        <button type="submit">View details</button>
                    </form>
                    <p>Planet selected: {this.state.inputName} das not exist</p>
                    <table>
                        <thead>
                            <tr>
                                <th>ord</th>
                                <th>name</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            {planete.slice(firstInPage, lastInPage).map(planeta =>
                                <tr>
                                    <td>{planeta.cardinal}</td>
                                    <td>{planeta.name}</td>
                                </tr>)}
                        </tbody>
                    </table>
                    <br />
                    <span><button onClick={this.backPage} id="back">back</button>
                        <button onClick={this.nextPage} id="next">next</button>
                        <button onClick={this.backToMenu}>Home</button>
                page: {this.state.currentPage}/{nrPages}</span>
                </center>
            </div>
        )
    }
    async componentDidMount() {
        await fetchData(BASE_URL)
        var nrOrd = 1;
        allData.shift();
        allData.forEach(function (item) { item.cardinal = nrOrd++ })
        this.setState({ planete: allData });
    }
}
export default PlanetTable
