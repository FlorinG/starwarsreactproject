import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import Resources from "../index.js";

var allData = ["dummy"];
var itemsPerPage = 15;
var nrPages = 1;
var firstInPage = 0;
var lastInPage = 0;
var pageCrt = 1;

const PEOPLE_URL = "https://swapi.py4e.com/api/people/";

async function fetchData(apiUrl) {
    while (apiUrl !== '' && apiUrl != null) {
        await fetch(apiUrl)
            .then(res => res.json())
            .then(data => {
                allData.push(...data.results);
                apiUrl = data['next'];
                console.log(apiUrl);

            })
    }
}

class UserTableAutonomous extends Component {

    state = {
        allPersons: [],
        personaje: [],
        currentPage: 1
    }


    constructor(props) {
        super(props);
        this.state = {
            allPersons: [],
            personaje: [],
            currentPage: 1,
            selectGenre: "all",
        };
    }

    nextPage = () => {

        console.log(this.state.currentPage)
        pageCrt = this.state.currentPage;
        if (this.state.currentPage < nrPages) {
            this.setState({ currentPage: pageCrt + 1 })
        }
    }

    backPage = () => {
        console.log(this.state.currentPage)
        pageCrt = this.state.currentPage;
        if (this.state.currentPage > 1) {
            this.setState({ currentPage: pageCrt - 1 })
        }
    }

    selectGenre = (e) => {
        this.setState({ selectGenre: e.target.value })
        console.log(e.target.value);
        console.log(this.state.selectGenre);
    }
    handleClick = () => {
        alert('button clicked');
    }

    backToMenu() {
        ReactDOM.render(<Resources />, document.getElementById("root"));
    }

    render() {

        var { allPersons } = this.state.personaje;
        if (this.state.selectGenre !== "all") {
            var personaje = this.state.allPersons.filter(pers => pers.gender === this.state.selectGenre);
        } else {
            var personaje = this.state.allPersons;
        }
        var lastItem = personaje.length - 1;
        nrPages = Math.ceil(personaje.length / itemsPerPage);
        console.log(nrPages);
        firstInPage = (this.state.currentPage - 1) * itemsPerPage;
        lastInPage = Math.min(firstInPage + itemsPerPage, lastItem);
        let unique = [...new Set(personaje.map(item => item.gender))];
        unique.unshift("all");
        console.log(unique);

        let optionList = unique.map((item) => {
            return <option name={item} value={item}>{item}</option>
        });
        return (
            <div id="listingTable">
                <center>
                    <h2>Peoples</h2>
                    <span><p>Filter by gender</p>
                        <form>
                            <select onChange={this.selectGenre}>
                                {optionList}
                            </select>
                        </form>

                    </span>
                    <table>
                        <thead>
                            <tr>
                                <th>ord</th>
                                <th>name</th>
                            </tr>
                        </thead>
                        <tbody id="persbody">
                            {personaje.slice(firstInPage, lastInPage).map(personaj =>
                                <tr>
                                    <td>{personaj.cardinal}</td>
                                    <td>{personaj.name}</td>
                                </tr>)}
                        </tbody>
                    </table>
                    <br />
                    <span><button onClick={this.backPage} id="back">prev</button>
                        <button onClick={this.nextPage} id="next">next</button>
                        <button onClick={this.backToMenu}>Home</button>
                        page: {this.state.currentPage}/{nrPages}</span>
                </center>
            </div>

        )
    }
    async componentDidMount() {
        await fetchData(PEOPLE_URL)
        var nrOrd = 1;
        allData.shift();
        allData.forEach(function (item) { item.cardinal = nrOrd++ })
        this.setState({ allPersons: allData })
        this.setState({ personaje: allData });
    }
}
export default UserTableAutonomous
